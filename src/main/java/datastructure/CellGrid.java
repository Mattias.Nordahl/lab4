package datastructure;


import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int columns;
    private CellState[][] cells;
     

    public CellGrid(int rows, int columns, CellState initialState) {
		
        if(rows <= 0 || columns <= 0)
            throw new IllegalArgumentException();
        this.rows = rows; 
        this.columns = columns;
        this.cells = new CellState[rows][columns]; 
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                cells[i][j] = initialState;
            }           
        }
	}

    @Override
    public int numRows() {
        
        return rows;
    }

    @Override
    public int numColumns() {
        
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        
        if(row < 0 || row >= numRows())
            throw new IndexOutOfBoundsException();
        if (column < 0 || column >= numColumns())
            throw new IndexOutOfBoundsException();
        cells[row][column] = element;
    }


    @Override
    public CellState get(int row, int column) {
        
        if(row < 0 || row >= numRows())
            throw new IndexOutOfBoundsException();
        if (column < 0 || column >= numColumns())
            throw new IndexOutOfBoundsException();
        return cells[row][column];
    }

    @Override
    public IGrid copy() {
        
        CellGrid gridcopy = new CellGrid(this.rows, this.columns, null);
        for (int i = 0; i < numRows(); i++) {
            for (int j = 0; j < numColumns(); j++) {
                gridcopy.set(i, j, this.get(i,j));
            }
        }

        return gridcopy;

    }
    
}
