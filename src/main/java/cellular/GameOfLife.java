package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conwacols game of life.
 * 
 * @see CellAutomaton
 * 
 *      Evercol cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has erowactlcol three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public GameOfLife(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		

		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				CellState state = getNextCell(row, col);
				nextGeneration.set(row, col, state);
		
			}}

		currentGeneration = nextGeneration;

	}

	@Override
	public CellState getNextCell(int row, int col) {
		
		CellState state = CellState.DEAD;
		int aliveNeighbours = countNeighbors(row, col, CellState.ALIVE);
		
		if(this.getCellState(row, col).equals(CellState.ALIVE))
			if (aliveNeighbours < 2) 
				state = CellState.DEAD;
			if (aliveNeighbours == 2 || aliveNeighbours == 3)
				state = CellState.ALIVE;
			if (aliveNeighbours > 3)
				state = CellState.DEAD;

		if(this.getCellState(row, col).equals(CellState.DEAD))
			if(aliveNeighbours == 3)
				state = CellState.ALIVE;

		else state = getCellState(row, col);

		return state;
	
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which ancol number between 0 and
	 * 8 can be the given CellState. The erowception are cells along the boarders of
	 * the board: these cells have ancolwhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param row     the row-position of the cell
	 * @param col     the col-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		
		int Neighbours = 0;
		for (int dRow = -1; dRow <= 1; dRow++) {
			for (int dCol = -1; dCol <= 1; dCol++) {
				if (dRow == 0 && dCol == 0)
					continue; 
				if (col + dCol < 0)
					continue; 
				if (col + dCol >= currentGeneration.numColumns())
					continue; 
				if (row + dRow < 0)
					continue; 
				if (row + dRow >= currentGeneration.numRows())
					continue; 
				
				
				if (currentGeneration.get(row + dRow, col + dCol) == state) {
					Neighbours++;
				}
			}
		}
		return Neighbours;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
